# bib-sort - Sort entries of a .bib file

bib-sort is a very simple program which leverages the `bibtex-ruby` gem
to sort the entries of a .bib file.

## Installation

```
gem build bib-sort.gemspec
gem install bib-sort*.gemspec --user
```

## Usage

`bib-sort FILE`

If you want to sort entry types as well (e.g. `@article` before `@book`),
pass it the `-t` flag.

By default, it will print the sorted .bib to the standard output. To perform
in-place replacement, pass it the `-i` flag.

## License

Everything in this repository is released under the
GNU Affero General Public License, version 3 or any later version, unless
otherwise specified (and even in that case, it's all free software).
