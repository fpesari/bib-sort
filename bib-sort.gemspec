# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'bib-sort'
  s.version       = '0.1.1'
  s.summary       = 'Sort entries of a .bib file'
  s.description   = <<~DESC
   Sort the entries of a BibTeX (LaTeX bibliography) .bib file.
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/bib-sort'
  s.files         = %w[.rubocop.yml bib-sort.gemspec LICENSE bin/bib-sort
                       README.md]
  s.executables   = %w[bib-sort]
  s.license       = 'AGPL-3.0+'
  s.add_runtime_dependency 'bibtex-ruby', '~>5.1', '>=5.1.5'
end
